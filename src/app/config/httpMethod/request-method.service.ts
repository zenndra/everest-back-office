import {Injectable} from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GlobalResponse} from '../../models/responses/global/global-response';
import {LoginResponse} from '../../models/responses/login/login-response';
import {UtilService} from '../../services/util/util.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RequestMethodService extends HttpClient {

  constructor(handler: HttpHandler, private router: Router, private http: HttpClient, private utilService: UtilService) {
    super(handler);
  }

  // @ts-ignore
  getMethod<T>(url: string, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<T>;
}
