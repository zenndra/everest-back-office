import {ErrorHandler, Injectable, Input} from '@angular/core';
import Swal from 'sweetalert2';
import {ErrorMessagesConstant} from '../../constants/error-messages-constant';

@Injectable()
export class GlobalErrorHandling implements ErrorHandler {

  constructor() {
  }

  handleError(error: any): void {
    var title = ErrorMessagesConstant.GLOBAL_ERROR_TITLE;
    var message = ErrorMessagesConstant.GLOBAL_ERROR_CONTENT;


    if (error.title != null && error.title != '') {
      console.log('kok ke sini');
      title = error.title;
    }
    if (error.message != null && error.message != '') {
      message = error.message;
    }

    Swal.fire(title, message, 'error');
  }

}
