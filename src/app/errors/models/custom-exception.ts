export class CustomException extends Error{
  title: string;
  case: string;
  status: number;
  message: string;

  constructor() {
    super();
  }
}
