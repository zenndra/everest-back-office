export class GlobalResponse<T> {
  success: boolean;
  message: string;
  data: T;

  constructor() {
  }
}
