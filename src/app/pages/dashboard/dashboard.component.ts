import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import {DashboardService} from '../../services/dashboard/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private dashboardService: DashboardService) {
  }

  ngOnInit() {

    $(document).ready(function() {
      $(document).on('click', '.cta', function() {
        $(this).toggleClass('active');
      });
    });

    $(document).ready(function() {
      $('.hamburger').click(function() {
        $('.sidebar-menu').removeClass('flowHide');
        $('.sidebar-menu').toggleClass('full-side-bar');
        $('.nav-link-name').toggleClass('name-hide');
      });
    });

    $(document).ready(function() {
      $('.nav-link').hover(function() {
        $('.sidebar-menu').removeClass('flowHide');
        $(this).addClass('tax-active');

      }, function() {
        $('.sidebar-menu')
          .addClass('flowHide');
        $(this).removeClass('tax-active');

      });
    });
  }

  signOut(){
    //call API sign out
    this.dashboardService.signOut();
  }

}
