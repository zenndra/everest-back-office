import {Component, Input, OnInit} from '@angular/core';
import {PublicService} from '../../services/public/public.service';
import {LoginRequest} from '../../models/requests/login/login-request';
import {LoginValidation} from '../../validations/input/login/login-validation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private publicService: PublicService) {
  }

  ngOnInit() {
  }

  login(username: string, password: string) {
    const request = new LoginRequest();
    request.username = username;
    request.password = password;
    const loginValidation = new LoginValidation(request);
    loginValidation.validate();
    this.publicService.login(request);
  }

}
