import {Injectable} from '@angular/core';
import {GlobalResponse} from '../../models/responses/global/global-response';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {PublicService} from '../public/public.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, private router: Router, private publicService: PublicService) {
  }

  signOut() {
    //
    return this.http.get<GlobalResponse<Object>>('http://localhost:8080/api/signOut')
      .subscribe(response => {
          this.publicService.utilService.clearLocalStorage();
        },
        error => {
          this.router.navigateByUrl('/loginPage');
        });
  }
}
