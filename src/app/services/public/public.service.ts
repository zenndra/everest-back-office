import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GlobalResponse} from '../../models/responses/global/global-response';
import {LoginRequest} from '../../models/requests/login/login-request';
import {GlobalValidation} from '../../validations/global/global-validation';
import {Router} from '@angular/router';
import {LoginResponse} from '../../models/responses/login/login-response';
import {UtilService} from '../util/util.service';
import {RequestMethodService} from '../../config/httpMethod/request-method.service';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  inputValidation: GlobalValidation[];

  constructor(private http: HttpClient, private router: Router, public utilService: UtilService, private httpMethod: RequestMethodService) {
  }

  login(loginModel: LoginRequest) {
    return this.http.post<GlobalResponse<LoginResponse>>('http://localhost:8080/api/test/login', loginModel)
      .subscribe(response => {
          this.utilService.saveToLocalStorage('token', response.data.token);
        },
        error => { //bukan 200 / 201
          console.error('IH ERROR');
          console.log(error);
          this.router.navigateByUrl('/dashboard');
        });
  }

  // login2(loginModel: LoginRequest) {
  //   return this.httpMethod.getMethod<GlobalResponse<LoginResponse>>('http://localhost:8080/api/test/login', false, loginModel)
  //     .subscribe(response => {
  //         this.utilService.saveToLocalStorage('token',response.data.token);
  //       },
  //       error => { //bukan 200 / 201
  //         console.error('IH ERROR');
  //         console.log(error);
  //         this.router.navigateByUrl('/dashboard');
  //       });
  // }

}
