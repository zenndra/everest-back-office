import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() {
  }

  saveToLocalStorage(key: string, token: string) {
    localStorage.setItem('token', token);
  }

  deleteLocalStorage(key) {
    localStorage.removeItem(key);
  }

  getLocalStorage(key) {
    localStorage.getItem(key);
  }

  clearLocalStorage() {
    localStorage.clear();
  }

  getHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    return headers.set('', '');
  }

  getHeadersBearer(): HttpHeaders {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    headers.set('Authorization', 'Bearer ' + this.getLocalStorage('token'));
    return headers;
  }


}
