export interface GlobalValidation {
  mandatory();

  validate();
}
