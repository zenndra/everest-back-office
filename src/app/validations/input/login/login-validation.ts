import {GlobalValidation} from '../../global/global-validation';
import {LoginRequest} from '../../../models/requests/login/login-request';
import {CustomException} from '../../../errors/models/custom-exception';
import {CaseConstant} from '../../../constants/case-constant';
import {ErrorMessagesConstant} from '../../../constants/error-messages-constant';

export class LoginValidation implements GlobalValidation {
  private loginRequest: LoginRequest;

  constructor(loginRequest: LoginRequest) {
    this.loginRequest = loginRequest;
  }

  mandatory() {
    var customException = new CustomException();
    customException.case = CaseConstant.CASE_LOGIN;
    customException.status = 401;
    customException.title = ErrorMessagesConstant.ERROR_LOGIN_TITLE;
    if (this.loginRequest.username === null || this.loginRequest.username === '') {
      customException.message = 'Mohon isi kolom Username';
      throw customException;
    }
    if (this.loginRequest.password === null || this.loginRequest.password === '') {
      customException.message = 'Mohon isi kolom Password';
      throw customException;
    }
  }

  validate() {
    this.mandatory();
  }

}
